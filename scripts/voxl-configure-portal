#!/bin/bash
################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

NAME="voxl-portal"
ME="voxl-configure-portal"
SERVICE_FILE="${NAME}.service"
USER=$(whoami)


print_usage () {
	echo ""
	echo "Start wizard with prompts:"
	echo "${ME}"
	echo ""
	echo "Shortcut configuration arguments for scripted setup."
	echo ""
	echo "${ME} disable"
	echo "${ME} enable"
	echo ""
	echo "show this help message:"
	echo "${ME} help"
	echo ""
	exit 0
}




disable_service_and_exit () {
	echo "disabling ${NAME} systemd service"
	systemctl disable ${SERVICE_FILE}
	echo "stopping ${NAME} systemd service"
	systemctl stop ${SERVICE_FILE}
	echo "Done configuring ${NAME}"
	exit 0
}

enable_service_and_exit () {
	echo "enabling  ${NAME} systemd service"
	systemctl enable  ${SERVICE_FILE}
	echo "starting  ${NAME} systemd service"
	systemctl restart  ${SERVICE_FILE}
	echo "Done configuring ${NAME}"
	exit 0
}


################################################################################
## actual start of execution, handle optional arguments first
################################################################################

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi


## convert argument to lower case for robustness
arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')

## parse arguments
case ${arg} in
	"")
		echo "Starting Wizard"
		;;
	"h"|"-h"|"help"|"--help")
		print_usage
		exit 0
		;;
	"disable")
		disable_service_and_exit
		;;
	"enable")
		enable_service_and_exit
		;;
	*)
		echo "invalid option"
		print_usage
		exit 1
esac

################################################################################
## no optional arguments, start config wizard prompts
################################################################################


echo " "
echo "do you want to enable or disable ${NAME}"
select opt in "enable" "disable"; do
case $opt in
enable )
	enable_service_and_exit
	break;;
disable )
	disable_service_and_exit
	break;;
*)
	echo "invalid option"
	esac
done


# all done!

