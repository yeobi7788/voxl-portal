#include "header_manager.h"

#include <modal_pipe.h>
#include <voxl_cutils.h>
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional but necessary for battery/gps/flight data
#include <c_library_v2/common/mavlink.h>
#endif

#include <algorithm>
#include <vector>

#include "manager_utils.h"
#include "page_manager.h"

#define PX4_STATUS_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_sys_status/")
#define MAVLINK_IO_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_mavlink_io/")
#define GPS_RAW_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_gps_raw_int/")

#define GPS_INDEX 0
#define BATT_INDEX 1
#define FLIGHT_INFO_INDEX 2

extern struct mg_mgr mgr_;

static websocket_info ws_info;
static int channels[3] = {false};

typedef struct flight_msg
{
    uint8_t msg_index;
    uint8_t armed;
    uint8_t main_mode;
    uint32_t sub_mode;
    char status_text[128];
} __attribute__((packed)) flight_msg;

static void _gps_connect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: gps connected\n");
}

static void _battery_connect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: battery connected\n");
}

static void _flight_info_connect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: flight_info connected\n");
}

static void _gps_disconnect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: gps disconnected\n");
}

static void _battery_disconnect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: battery disconnected\n");
}

static void _flight_info_disconnect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: flight_info disconnected\n");
}

static void GpsDataCallback(__attribute__((unused)) int ch, char *data, int bytes, __attribute__((unused)) void *context)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it
    static uint8_t gps_data[2] = {GPS_INDEX, 0};

    if (!ws_info.connected)
        return;

    // validate that the data makes sense
    if (bytes != sizeof(mavlink_message_t))
        return;
    mavlink_message_t *msg = (mavlink_message_t *)data;
    if (msg->magic != MAVLINK_STX)
    {
        fprintf(stderr, "ERROR bad magic number\n");
        return;
    }

    gps_data[1] = mavlink_msg_gps_raw_int_get_satellites_visible(msg);

    struct mg_connection *c;
    for (c = mgr_.conns; c != NULL; c = c->next)
    {
        if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id)))
        {
            continue;
        }
        mg_ws_send(c, (char *)gps_data, sizeof(gps_data), WEBSOCKET_OP_BINARY);
    }
    sleep(3);
#endif // ENABLE_MAVLINK_SUPPORT
    return;
}

static void BattDataCallback(__attribute__((unused)) int ch, char *data, int bytes, __attribute__((unused)) void *context)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it
    static uint8_t batt_data[2] = {BATT_INDEX, 0};

    if (!ws_info.connected)
        return;

    // validate that the data makes sense
    if (bytes != sizeof(mavlink_message_t))
        return;
    mavlink_message_t *msg = (mavlink_message_t *)data;
    if (msg->magic != MAVLINK_STX)
    {
        fprintf(stderr, "ERROR bad magic number\n");
        return;
    }

    batt_data[1] = mavlink_msg_sys_status_get_battery_remaining(msg);

    struct mg_connection *c;
    for (c = mgr_.conns; c != NULL; c = c->next)
    {
        if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id)))
        {
            continue;
        }
        mg_ws_send(c, (char *)batt_data, sizeof(batt_data), WEBSOCKET_OP_BINARY);
    }

    sleep(3);
#endif // ENABLE_MAVLINK_SUPPORT
    return;
}

static void FlightInfoCallback(__attribute__((unused)) int ch, char *data, int bytes, __attribute__((unused)) void *context)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it
    static flight_msg curr_msg;

    if (!ws_info.connected)
        return;

    // validate that the data makes sense
    if (bytes != sizeof(mavlink_message_t))
        return;
    mavlink_message_t *msg = (mavlink_message_t *)data;
    if (msg->magic != MAVLINK_STX)
    {
        fprintf(stderr, "ERROR bad magic number\n");
        return;
    }

    curr_msg.msg_index = FLIGHT_INFO_INDEX;

    // handle mavlink error messages
    if (msg->msgid == MAVLINK_MSG_ID_STATUSTEXT)
    {
        char *text = (char *)&_MAV_PAYLOAD(msg)[1]; // pointer to text in the mavlink msg
        memset(&curr_msg, 0, 48);
        snprintf(curr_msg.status_text, sizeof(curr_msg.status_text), "%s", text);
    }
    else if (msg->msgid == MAVLINK_MSG_ID_HEARTBEAT)
    {
        bool armed = (mavlink_msg_heartbeat_get_base_mode(msg) & MAV_MODE_FLAG_SAFETY_ARMED);
        uint8_t main_mode = (mavlink_msg_heartbeat_get_custom_mode(msg) & 0x00FF0000) >> 16;
        uint32_t sub_mode = (mavlink_msg_heartbeat_get_custom_mode(msg) & 0xFF000000) >> 24;

        curr_msg.armed = armed;
        curr_msg.main_mode = main_mode;
        curr_msg.sub_mode = sub_mode;
    }
    else
        return;

    struct mg_connection *c;
    for (c = mgr_.conns; c != NULL; c = c->next)
    {
        if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id)))
        {
            continue;
        }
        mg_ws_send(c, (char *)&curr_msg, sizeof(curr_msg), WEBSOCKET_OP_BINARY);
    }
#endif // ENABLE_MAVLINK_SUPPORT
    return;
}

void HeaderManagerCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional but necessary for battery/gps/flight data

    if (c->is_closing)
    {
        // Remove this connection from our connection ids
        ws_info.connection_ids.erase(std::remove(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id), ws_info.connection_ids.end());

        // Close the pipe if no longer sending to any websockets
        if (ws_info.connection_ids.empty())
        {
            ws_info.connected = false;

            for (size_t i = 0; i < 3; i++)
                pipe_client_close(channels[i]);
        }
        return;
    }

    if (!ws_info.connected)
    {
        ws_info.connected = true;

        int gps_ch = pipe_client_get_next_available_channel();

        pipe_client_set_connect_cb(gps_ch, _gps_connect_cb, NULL);
        pipe_client_set_disconnect_cb(gps_ch, _gps_disconnect_cb, NULL);
        pipe_client_set_simple_helper_cb(gps_ch, GpsDataCallback, c);
        int ret = pipe_client_open(gps_ch, GPS_RAW_PATH, PROCESS_NAME,
                                   EN_PIPE_CLIENT_SIMPLE_HELPER, (sizeof(mavlink_message_t) * 5));

        channels[GPS_INDEX] = gps_ch;

        int batt_ch = pipe_client_get_next_available_channel();

        pipe_client_set_connect_cb(batt_ch, _battery_connect_cb, NULL);
        pipe_client_set_disconnect_cb(batt_ch, _battery_disconnect_cb, NULL);
        pipe_client_set_simple_helper_cb(batt_ch, BattDataCallback, c);
        ret = pipe_client_open(batt_ch, PX4_STATUS_PATH, PROCESS_NAME,
                               EN_PIPE_CLIENT_SIMPLE_HELPER, (sizeof(mavlink_message_t) * 5));

        channels[BATT_INDEX] = batt_ch;

        int flight_ch = pipe_client_get_next_available_channel();

        pipe_client_set_connect_cb(flight_ch, _flight_info_connect_cb, NULL);
        pipe_client_set_disconnect_cb(flight_ch, _flight_info_disconnect_cb, NULL);
        pipe_client_set_simple_helper_cb(flight_ch, FlightInfoCallback, c);
        ret = pipe_client_open(flight_ch, MAVLINK_IO_PATH, PROCESS_NAME,
                               EN_PIPE_CLIENT_SIMPLE_HELPER, (sizeof(mavlink_message_t) * 5));

        channels[FLIGHT_INFO_INDEX] = flight_ch;
    }

    ws_info.connection_ids.push_back(c->id);
#endif // ENABLE_MAVLINK_SUPPORT
}
