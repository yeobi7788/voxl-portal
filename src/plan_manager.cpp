#include "plan_manager.h"

#include <modal_pipe.h>
#include <stdlib.h>
#include <voxl_cutils.h>

#include <algorithm>

#include "page_manager.h"
#include "manager_utils.h"

// this one is just for sending control cmds back to voxl-mapper
#define PLAN_NAME "plan_msgs"
#define PLAN_LOCATION (MODAL_PIPE_DEFAULT_BASE_DIR PLAN_NAME "/")

// this is for rendering paths, debug points, etc
#define RENDER_NAME "voxl_planner_render"
#define RENDER_LOCATION (MODAL_PIPE_DEFAULT_BASE_DIR RENDER_NAME "/")

extern struct mg_mgr mgr_;

// this manager will subscribe to two channels, but only communicates with one
static websocket_info ws_info;

static void _connect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: plan connected\n");
}

static void _disconnect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: plan disconnected\n");
}

static void PlanDataCallback(int ch, point_cloud_metadata_t meta, void *data, void *context)
{
    static char *plan_data = nullptr;

    if (!ws_info.connected)
        return;

    struct mg_connection *c;

    int plan_meta_size = sizeof(point_cloud_metadata_t);
    int plan_body_size = pipe_point_cloud_meta_to_size_bytes(meta);
    int plan_total_size = plan_meta_size + plan_body_size;

    plan_data = (char *)malloc(plan_total_size);
    memcpy(plan_data, &meta, plan_meta_size);
    memcpy(plan_data + plan_meta_size, data, plan_body_size);

    for (unsigned int i = 0; i < ws_info.connection_ids.size(); i++)
    {
        for (c = mgr_.conns; c != NULL; c = c->next)
        {
            if (c->id != ws_info.connection_ids[i])
                continue;
            // check if we are backing up the socket
            // if so, just drop the packet for this connection, but mark sent as true to prevent closure
            if (c->send.len != 0 || c->recv.len != 0)
            {
                // fprintf(stderr, "dropping plan packet\n");
                continue;
            }

            mg_ws_send(c, plan_data, plan_total_size, WEBSOCKET_OP_BINARY);
        }
    }
    free(plan_data);
    plan_data = nullptr;
}

void PlanManagerCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{

    if (c->is_closing)
    {
        // Remove this connection from our connection ids
        ws_info.connection_ids.erase(std::remove(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id), ws_info.connection_ids.end());

        // Close the pipe if no longer sending to any websockets
        if (ws_info.connection_ids.empty())
        {
            ws_info.connected = false;
            pipe_client_close(ws_info.ch);
        }
        return;
    }

    if (!ws_info.connected)
    {
        ws_info.connected = true;
        int plan_ch = pipe_client_get_next_available_channel();
        pipe_client_set_connect_cb(plan_ch, _connect_cb, NULL);
        pipe_client_set_disconnect_cb(plan_ch, _disconnect_cb, NULL);
        if (int ret = pipe_client_open(plan_ch, PLAN_LOCATION, PROCESS_NAME, 0, 1024 * 128 * 64))
        {
            pipe_print_error(ret);
            fprintf(stderr, "\n\nFailed to open pipe: %s\n\n\n", PLAN_LOCATION);
        }

        int ch = pipe_client_get_next_available_channel();
        pipe_client_set_connect_cb(ch, _connect_cb, NULL);
        pipe_client_set_disconnect_cb(ch, _disconnect_cb, NULL);
        pipe_client_set_point_cloud_helper_cb(ch, PlanDataCallback, NULL);
        if (int ret = pipe_client_open(ch, RENDER_LOCATION, PROCESS_NAME,
                                       CLIENT_FLAG_EN_POINT_CLOUD_HELPER, 1024 * 1024 * 64))
        {
            pipe_print_error(ret);
            fprintf(stderr, "\n\nFailed to open pipe: %s\n\n\n", RENDER_LOCATION);
        }

        ws_info.ch = ch;
    }

    ws_info.connection_ids.push_back(c->id);
}
